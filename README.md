# Present Chat #
### What is this repository for? ###

* To track the development of the C# / WPF extensible multi-protocol chat client Present Chat.
* The client currently only supports the IRC protocol and is not currently extensible.
* The extensible interface will be provided through the Managed Extensibility Framework

### Long term project goals ###

* Provide easy extensibility with simple and limited MEF interfaces
* Create a next-gen chat client that is capable of a lot more than just sending and receiving messages.
* Utilize Machine Learning APIs to analyze chat and provide alternative perspectives on your communities.
    * This would be things like aggregating pasted images and associating them with users, analyzing their content and creating the concept of interests for users
    * Provide language translation services to allow multi-lingual conversations
    * Learn who the users friends are, notify user when friends are online, offline, active, or inactive.
    * Learn to associate users between multiple chat services
        * Does an IRC user have a Steam account?  Attempt to associate them so you can see Steam details for IRC users.
            * Ideally we would build this relationship between all available services
            * Allow the system to learn how you like to conduct various messaging tasks.  ex. Do you prefer to use Google Talk to private message IRC users if available?

* Use machine learning to discover users interests, and highlight chat logs to enunciate topics of interest.
    * Create "long term" and "short term" memory of interests

### Setup and Notes ###

* Clone Repository
* Open Repository in Visual Studio
    * *Project lead is currently developing in Visual Studio 2012 and Visual Studio 2015.  There are no known compatibility issues.*
* Enable NuGet Package Restore
* Build and Launch Project
* Enter a Profile name and click save to get started. 

### Contribution guidelines ###

* This program is designed using the Model View ViewModel convention.  Pull Requests that doesn't adhere to this convention will need to be re-implemented using this design pattern. Please examine code in the ViewModel and View directories for guidance.
    * This software is built with a focus on Zero Code Behind MVVM. This specifically excludes writing DependencyProperties in UserControl code-behind.
    * Command implementation is handled through Attributes with a pre-compile FODY Add-in: Commander.Fody
    * INotifyPropertyChanged is implemented through Attributes with a pre-compile FODY Add-in: PropertyChanged.Fody

* Due to Commander.FODY, Binding to commands will show up as invalid.
    * We maybe want to reach out to ReSharper to have them investigate this use case to see if they can resolve the problem.
    * All commands should attempt to be built using Commander.Fody Attributes [OnCommand()] and [OnCommandCanExecute()]
        * There are exceptions to this where this system will not provide the necessary features.

* Code is written with Visual Studio and ReSharper, all pull requests should aim to be clean of ReSharper complaints or will need to be re factored before being merged.
    * This is not a hard requirement, but it is preferred.  If there are complaints, depending on now significant, the pull request merge could be delayed.

* There are some extra unnecessary items in the NuGet Restore that need to be cleaned out, please do not commit these additional items in the mean time.
    * AutoDependencyProperty.Fody - This doesn't appear to work. Further investigation into making this tool work is warranted for code cleanliness

### Attribution ###

Library              | Repository                                     | License
---------------------|------------------------------------------------|-----------
SmartIrc4Net         | https://github.com/meebey/SmartIrc4net         | LGPL
Fody                 | https://github.com/Fody/Fody                   | MIT
Commander.Fody       | https://github.com/DamianReeves/Commander.Fody | MIT
PropertyChanged.Fody | https://github.com/Fody/PropertyChanged        | MIT
Log4Net              | http://logging.apache.org/log4net/index.html   | Apache License, Version 2.0
NewtonSoft JSON.NET  | http://www.newtonsoft.com/json                 | MIT
FuzzyString          | https://fuzzystring.codeplex.com/              | Eclipse Public License
ColorWheel           | http://color.codeplex.com/                     | New BSD License (BSD)

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### License - MIT License ###

Copyright (c) 2015 James LaPenn

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.