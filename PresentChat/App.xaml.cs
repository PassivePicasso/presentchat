﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Threading;
using Commander;
using PresentChat.View;

namespace PresentChat
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        public static readonly string SettingsDirectory = Path.Combine(GetLocalDirectory(), "Settings");
        public static readonly string ProfileDirectory = Path.Combine(SettingsDirectory, "Profile");
        public static readonly string ThemeDirectory = Path.Combine(SettingsDirectory, "Themes");

        private static MainWindow mainWindow;

        public App()
        {
            if (!Directory.Exists(SettingsDirectory)) Directory.CreateDirectory(SettingsDirectory);
            if (!Directory.Exists(ProfileDirectory)) Directory.CreateDirectory(ProfileDirectory);
            if (!Directory.Exists(ThemeDirectory)) Directory.CreateDirectory(ThemeDirectory);
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            ImportMessageTemplates();

            mainWindow = new MainWindow();
            mainWindow.Show();
        }

        [OnCommand("ReImportTemplatesCommand")]
        public void ImportMessageTemplates()
        {
            var info = GetRemoteStream(new Uri("/Settings/Themes/MessageTemplates.xaml", UriKind.Relative));
            var messageTemplatesDictionary = (ResourceDictionary)new XamlReader().LoadAsync(info.Stream);
            Resources["MessageTemplates"] = messageTemplatesDictionary;

            if (mainWindow != null)
            {
                Dispatcher.Invoke(DispatcherPriority.ApplicationIdle, new Action(() =>
                {
                    var newWindow = new MainWindow();
                    newWindow.Show();
                    newWindow.Top = mainWindow.Top;
                    newWindow.Left = mainWindow.Left;
                    newWindow.Width = mainWindow.Width;
                    newWindow.Height = mainWindow.Height;
                    newWindow.WindowState = mainWindow.WindowState;
                    mainWindow.Close();
                    mainWindow = newWindow;
                }));
            }
        }

        private static string GetLocalDirectory()
        {
            var directoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
            return directoryName != null ? directoryName.Replace("file:\\", "") : string.Empty;
        }
    }
}
