﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace PresentChat.View.CustomControls
{
    public class ResizeControl : Thumb
    {
        public enum Direction { Vertical, Horizontal, VerticalAndHorizontal }
        public static readonly DependencyProperty ResizeDirectionProperty = DependencyProperty.Register("ResizeDirection", typeof(Direction), typeof(ResizeControl), new FrameworkPropertyMetadata(default(Direction), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty TargetProperty = DependencyProperty.Register("Target", typeof (FrameworkElement), typeof (ResizeControl), new PropertyMetadata(default(FrameworkElement)));

        public Direction ResizeDirection { get { return (Direction)GetValue(ResizeDirectionProperty); } set { SetValue(ResizeDirectionProperty, value); } }

        public FrameworkElement Target
        {
            get { return (FrameworkElement) GetValue(TargetProperty); }
            set { SetValue(TargetProperty, value); }
        }

        public ResizeControl()
        {
            DragDelta += OnDragDelta;
        }

        private void OnDragDelta(object sender, DragDeltaEventArgs delta)
        {
            if (Target == null) return;
            switch (ResizeDirection)
            {
                case Direction.Vertical:
                    UpdateHeight(delta);
                    break;
                case Direction.Horizontal:
                    UpdateWidth(delta);
                    break;
                case Direction.VerticalAndHorizontal:
                    UpdateHeight(delta);
                    UpdateWidth(delta);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void UpdateWidth(DragDeltaEventArgs delta)
        {
            var newWidth = Target.ActualWidth + delta.HorizontalChange;
            newWidth = Math.Max(newWidth, 0);
            Target.Width = newWidth;
        }

        private void UpdateHeight(DragDeltaEventArgs delta)
        {
            var newHeight = Target.ActualHeight + delta.VerticalChange;
            newHeight = Math.Max(newHeight, 0);
            Target.Height = newHeight;
        }
    }
}
