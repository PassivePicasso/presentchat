﻿using System;
using FuzzyString;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Commander;
using System.Windows.Controls.Primitives;

namespace PresentChat.View.CustomControls
{
    public enum MatchMode { StartsWith, Contains, EndsWith, Fuzzy };
    /// <summary>
    /// Acquired from http://blog.pixelingene.com/2010/10/tokenizing-control-convert-token-to-tokens/
    /// </summary>
    public class SuggestReplaceControl : TextBox
    {
        public static readonly DependencyProperty SuggestionsProperty = DependencyProperty.Register("Suggestions", typeof(IList), typeof(SuggestReplaceControl), new FrameworkPropertyMetadata(default(IList), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty SuggestionOptionsProperty = DependencyProperty.Register("SuggestionOptions", typeof(IList), typeof(SuggestReplaceControl), new FrameworkPropertyMetadata(default(IList), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty SuggestionMemberProperty = DependencyProperty.Register("SuggestionMember", typeof(string), typeof(SuggestReplaceControl), new FrameworkPropertyMetadata(default(string), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty MatchModeProperty = DependencyProperty.Register("MatchMode", typeof(MatchMode), typeof(SuggestReplaceControl), new FrameworkPropertyMetadata(default(MatchMode), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty HasSuggestionsProperty = DependencyProperty.Register("HasSuggestions", typeof(bool), typeof(SuggestReplaceControl), new PropertyMetadata(default(bool)));
        public static readonly DependencyProperty SuggestionRectProperty = DependencyProperty.Register("SuggestionRect", typeof(Rect), typeof(SuggestReplaceControl), new PropertyMetadata(default(Rect)));
        public static readonly DependencyProperty NormalizedMatchThresholdProperty = DependencyProperty.Register("NormalizedMatchThreshold", typeof(double), typeof(SuggestReplaceControl), new PropertyMetadata(4.0));
        public static readonly DependencyProperty ResultLimitProperty = DependencyProperty.Register("ResultLimit", typeof(int), typeof(SuggestReplaceControl), new PropertyMetadata(int.MaxValue));
        public static readonly DependencyProperty ResultSelectorProperty = DependencyProperty.Register("ResultSelector", typeof(Selector), typeof(SuggestReplaceControl), new PropertyMetadata(default(Selector)));

        #region Properties

        public int ResultLimit
        {
            get { return (int)GetValue(ResultLimitProperty); }
            set { SetValue(ResultLimitProperty, value); }
        }

        public Selector ResultSelector
        {
            get { return (Selector)GetValue(ResultSelectorProperty); }
            set { SetValue(ResultSelectorProperty, value); }
        }

        public double NormalizedMatchThreshold
        {
            get { return (double)GetValue(NormalizedMatchThresholdProperty); }
            set { SetValue(NormalizedMatchThresholdProperty, value); }
        }

        public IList Suggestions
        {
            get { return (IList)GetValue(SuggestionsProperty); }
            set { SetValue(SuggestionsProperty, value); }
        }

        public IList SuggestionOptions
        {
            get { return (IList)GetValue(SuggestionOptionsProperty); }
            set { SetValue(SuggestionOptionsProperty, value); }
        }

        public string SuggestionMember
        {
            get { return (string)GetValue(SuggestionMemberProperty); }
            set { SetValue(SuggestionMemberProperty, value); }
        }

        public bool HasSuggestions
        {
            get { return (bool)GetValue(HasSuggestionsProperty); }
            set { SetValue(HasSuggestionsProperty, value); }
        }
        public MatchMode MatchMode
        {
            get { return (MatchMode)GetValue(MatchModeProperty); }
            set { SetValue(MatchModeProperty, value); }
        }

        public Rect SuggestionRect
        {
            get { return (Rect)GetValue(SuggestionRectProperty); }
            set { SetValue(SuggestionRectProperty, value); }
        }

        #endregion

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;

            var textBinding = GetBindingExpression(TextProperty);
            if (textBinding == null) return;
            textBinding.UpdateSource();
        }

        private PropertyInfo SelectionMemberGetter;
        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            if (SuggestionOptions == null || SuggestionOptions.Count == 0) return;
#warning we will probably want to watch for SelectionMember changing and update this
            if (SelectionMemberGetter == null && !string.IsNullOrEmpty(SuggestionMember))
                SelectionMemberGetter = SuggestionOptions[0].GetType().GetProperty(SuggestionMember);

            if (SelectionMemberGetter == null) return;
            if (Suggestions == null)
                SetCurrentValue(SuggestionsProperty, new List<object>());

            var worddata = GetWordData();
            var word = worddata.Item1;
            int wordIndex = worddata.Item2;

            var data = new object[SuggestionOptions.Count];
            SuggestionOptions.CopyTo(data, 0);
            if (string.IsNullOrEmpty(word))
            {
                SetCurrentValue(SuggestionsProperty, new List<object>());
                SetCurrentValue(HasSuggestionsProperty, false);
            }
            else
            {
                IEnumerable<object> suggests;

                if (MatchMode == MatchMode.Fuzzy)
                {
                    suggests = data.Select(so =>
                    {
                        var option = SelectionMemberGetter.GetValue(so, null).ToString();
                        var confidenceMetric = option.ToLower().LongestCommonSubsequence(word.ToLower());
                        return Tuple.Create(confidenceMetric.Length, so);
                    })
                    .Where(t => t.Item1 > NormalizedMatchThreshold)
                    .OrderByDescending(t => t.Item1)
                    .Select(t => t.Item2);
                }
                else
                {
                    suggests = data.Where(so =>
                    {
                        switch (MatchMode)
                        {
                            case MatchMode.StartsWith: return SelectionMemberGetter.GetValue(so, null).ToString().StartsWith(word);
                            case MatchMode.Contains: return SelectionMemberGetter.GetValue(so, null).ToString().Contains(word);
                            case MatchMode.EndsWith: return SelectionMemberGetter.GetValue(so, null).ToString().EndsWith(word);
                        }
                        return false;
                    });
                }
                suggests = suggests.Take(ResultLimit).ToList();

                SetCurrentValue(SuggestionsProperty, suggests);
                SetCurrentValue(HasSuggestionsProperty, suggests.Any());

                SetCurrentValue(SuggestionRectProperty, GetRectFromCharacterIndex(wordIndex, false));

                ResultSelector.SelectedIndex = 0;
            }
        }

        private Tuple<string, int> GetWordData()
        {
            var prevSelectStart = SelectionStart;
            var prevSelectLength = SelectionLength;
            int cursorPosition = SelectionStart;
            int nextSpace = Text.IndexOf(' ', cursorPosition);
            int selectionStart = 0;
            string trimmedString = string.Empty;
            if (nextSpace != -1)
            {
                trimmedString = Text.Substring(0, nextSpace);
            }
            else
            {
                trimmedString = Text;
            }


            if (trimmedString.LastIndexOf(' ') != -1)
            {
                selectionStart = 1 + trimmedString.LastIndexOf(' ');
                trimmedString = trimmedString.Substring(1 + trimmedString.LastIndexOf(' '));
            }

            SelectionStart = selectionStart;
            SelectionLength = trimmedString.Length;
            var word = SelectedText;
            SelectionStart = prevSelectStart;
            SelectionLength = prevSelectLength;

            return Tuple.Create(word, selectionStart);
        }

        [OnCommand("UpdateCurrentRun")]
        public void ReplaceLastRunWithText(string text)
        {
            if (string.IsNullOrEmpty(text)) text = string.Empty;

            var worddata = GetWordData();
            var word = worddata.Item1;
            var wordIndex = worddata.Item2;

            //This is to clear suggestions to prevent accidental double selection.
            if (!text.EndsWith(" "))
            {
                text += " ";
            }

            // Add text to the text
            Text = Text.Remove(wordIndex, word.Length);
            SetCurrentValue(TextProperty, Text.Insert(wordIndex, text));

            // Move the caret to the end of the added text
            CaretIndex = wordIndex + text.Length;

            // Move focus back to the text box. 
            Focus();

            SetCurrentValue(SuggestionsProperty, new List<object>());
            SetCurrentValue(HasSuggestionsProperty, false);
        }

        [OnCommand("ClearCommand")]
        public void Clear()
        {
            SetCurrentValue(TextProperty, string.Empty);
        }
    }
}