﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Documents;

namespace PresentChat.View.CustomControls.FlowDocuments
{
    public class InlineLinksRun : Span
    {
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof (string), typeof (InlineLinksRun), new FrameworkPropertyMetadata(default(string), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, TextChanged));

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        private static void TextChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
        {
            var span = dependencyObject as InlineLinksRun;
            if (span == null || args.NewValue == null) return;
            span.ProcessText(args.NewValue.ToString());
        }

        private void ProcessText(string message)
        {
            Inlines.Clear();
            var match = Regex.Match(message, @"((http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?)");
            var index = 0;
            while (match.Success)
            {
                var g = match.Groups[0];

                Inlines.Add(new Run(message.Substring(index, g.Index - index)) { Foreground = Foreground });

                var hyperlink = new Hyperlink(new Run(g.Value)) { NavigateUri = new Uri(g.Value) };
                hyperlink.RequestNavigate += (sender, args) => Process.Start(args.Uri.OriginalString);
                Inlines.Add(hyperlink);

                index = g.Index + g.Length;

                match = match.NextMatch();
            }
            Inlines.Add(new Run(message.Substring(index)) { Foreground = Foreground });
        }

    }
}
