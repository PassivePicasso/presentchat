﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows.Media;
using ColorWheel.Core;
using Commander;
using PresentChat.Annotations;
using PropertyChanged;

namespace PresentChat.View.CustomControls
{
    [DataContract]
    [ImplementPropertyChanged]
    public class Theme : INotifyPropertyChanged
    {
        private static List<PropertyInfo> brushProperties;

        [DoNotNotify]
        public Palette Palette { get; set; }

        [DataMember]
        public SolidColorBrush UiTextBrush { get; set; }
        [DataMember]
        public SolidColorBrush PendingUiTextBrush { get; set; }
        [DataMember]
        public SolidColorBrush BorderBrush { get; set; }
        [DataMember]
        public SolidColorBrush ActivatedBrush { get; set; }
        [DataMember]
        public SolidColorBrush MessageBrush { get; set; }
        [DataMember]
        public SolidColorBrush TextFieldBackgroundBrush { get; set; }
        [DataMember]
        public SolidColorBrush MainBackgroundBrush { get; set; }
        [DataMember]
        public SolidColorBrush HighlightBrush { get; set; }
        [DataMember]
        public SolidColorBrush NicknameBrush { get; set; }

        [DataMember]
        public SolidColorBrush TimeBrush { get; set; }

        private bool initialized = false;

        public Theme()
        {
            if (brushProperties == null)
                brushProperties = GetType().GetProperties().Where(pi => pi.PropertyType == typeof(SolidColorBrush)).ToList();

            Palette = Palette.Create(new RGBColorWheel(), Colors.White, PaletteSchemaType.Custom, brushProperties.Count);
            UpdateColors();
        }

        [OnDeserialized]
        public void DeserializedHandled(StreamingContext context)
        {
            for (int i = 0; i < brushProperties.Count; i++)
            {
                if (!initialized)
                    Palette.Colors[i].Name = brushProperties[i].Name;

                Palette.Colors[i].RgbColor = ((SolidColorBrush)brushProperties[i].GetValue(this)).Color;
            }
            initialized = true;
            UpdateColors();
        }

        [OnCommand("UpdateColorsCommand")]
        public void UpdateColors()
        {
            for (int i = 0; i < brushProperties.Count; i++)
            {
                var rgbColor = Palette.Colors[i].RgbColor;
                if (!initialized)
                {
                    Palette.Colors[i].Name = brushProperties[i].Name;
                    var solidColorBrush = new SolidColorBrush(rgbColor);
                    brushProperties[i].SetValue(this, solidColorBrush);
                    OnPropertyChanged(Palette.Colors[i].Name);
                }
                else
                {
                    var scb = brushProperties[i].GetValue(this) as SolidColorBrush;
                    if (scb == null || scb.Color.Equals(rgbColor)) continue;
                    scb.Color = rgbColor;
                    OnPropertyChanged(Palette.Colors[i].Name);
                }
            }
            initialized = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
