﻿using System.Windows;
using System.Windows.Input;
using Commander;

namespace PresentChat.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void StartDrag(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
