﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Media;

namespace PresentChat.View
{
    public static class ColorGenerator
    {
        private static Random random = new Random();

        // RYB color space
        private static class RYB
        {
            private static readonly double[] White = { 1, 1, 1 };
            private static readonly double[] Red = { 1, 0, 0 };
            private static readonly double[] Yellow = { 1, 1, 0 };
            private static readonly double[] Blue = { 0.163, 0.373, 0.6 };
            private static readonly double[] Violet = { 0.5, 0, 0.5 };
            private static readonly double[] Green = { 0, 0.66, 0.2 };
            private static readonly double[] Orange = { 1, 0.5, 0 };
            private static readonly double[] Black = { 0.2, 0.094, 0.0 };

            public static double[] ToRgb(double r, double y, double b)
            {
                var rgb = new double[3];
                for (int i = 0; i < 3; i++)
                {
                    rgb[i] = White[i] * (1.0 - r) * (1.0 - b) * (1.0 - y) +
                             Red[i] * r * (1.0 - b) * (1.0 - y) +
                             Blue[i] * (1.0 - r) * b * (1.0 - y) +
                             Violet[i] * r * b * (1.0 - y) +
                             Yellow[i] * (1.0 - r) * (1.0 - b) * y +
                             Orange[i] * r * (1.0 - b) * y +
                             Green[i] * (1.0 - r) * b * y +
                             Black[i] * r * b * y;
                }

                return rgb;
            }
        }

        private class Points : IEnumerable<double[]>
        {
            private readonly int pointsCount;
            private double[] picked;
            private int pickedCount;

            private readonly List<double[]> points = new List<double[]>();

            public Points(int count)
            {
                pointsCount = count;
            }

            private void Generate()
            {
                points.Clear();
                var numBase = (int)Math.Ceiling(Math.Pow(pointsCount, 1.0 / 3.0));
                var ceil = (int)Math.Pow(numBase, 3.0);
                for (int i = 0; i < ceil; i++)
                {
                    points.Add(new[]
                    {
                        Math.Floor(i/(double)(numBase*numBase))/ (numBase - 1.0),
                        Math.Floor((i/(double)numBase) % numBase)/ (numBase - 1.0),
                        Math.Floor((double)(i % numBase))/ (numBase - 1.0),
                    });
                }
            }

            private double Distance(double[] p1)
            {
                double distance = 0;
                for (int i = 0; i < 3; i++)
                {
                    distance += Math.Pow(p1[i] - picked[i], 2.0);
                }

                return distance;
            }

            private double[] Pick()
            {
                if (picked == null)
                {
                    picked = points[0];
                    points.RemoveAt(0);
                    pickedCount = 1;
                    return picked;
                }

                var d1 = Distance(points[0]);
                int i1 = 0, i2 = 0;
                foreach (var point in points)
                {
                    var d2 = Distance(point);
                    if (d1 < d2)
                    {
                        i1 = i2;
                        d1 = d2;
                    }

                    i2 += 1;
                }

                var pick = points[i1];
                points.RemoveAt(i1);

                for (int i = 0; i < 3; i++)
                {
                    picked[i] = (pickedCount * picked[i] + pick[i]) / (pickedCount + 1.0);
                }

                pickedCount += 1;
                return pick;
            }

            public IEnumerator<double[]> GetEnumerator()
            {
                Generate();
                for (int i = 0; i < pointsCount; i++)
                {
                    yield return Pick();
                }
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

        public static IEnumerable<Color> Generate(int numOfColors)
        {
            var points = new Points(numOfColors);

            foreach (var point in points)
            {
                var rgb = RYB.ToRgb(point[0], point[1], point[2]);
                yield return Color.FromArgb(255, (byte)Math.Floor(255 * rgb[0]), (byte)Math.Floor(255 * rgb[1]), (byte)Math.Floor(255 * rgb[2]));
            }
        }

        public static IEnumerable<Color> Generate(int numOfColors, double minHue, double maxHue, double minSaturation = 0, double maxSaturation = 1, double minValue = 0, double maxValue = 1)
        {
            for (int i = numOfColors; i > 0; i++)
            {
                yield return FromHSV(NextDouble(minHue, maxHue), NextDouble(minSaturation, maxSaturation), NextDouble(minValue, maxValue));
            }
        }

        private static double NextDouble(double min, double max)
        {
            return min + (random.NextDouble() * (max - min));
        }

        public static Color FromHSV(double h, double S, double V)
        {
            int r, g, b;

            double H = h;
            while (H < 0) { H += 360; };
            while (H >= 360) { H -= 360; };
            double R, G, B;
            if (V <= 0)
            { R = G = B = 0; }
            else if (S <= 0)
            {
                R = G = B = V;
            }
            else
            {
                double hf = H / 60.0;
                int i = (int)Math.Floor(hf);
                double f = hf - i;
                double pv = V * (1 - S);
                double qv = V * (1 - S * f);
                double tv = V * (1 - S * (1 - f));
                switch (i)
                {

                    // Red is the dominant color

                    case 0:
                        R = V;
                        G = tv;
                        B = pv;
                        break;

                    // Green is the dominant color

                    case 1:
                        R = qv;
                        G = V;
                        B = pv;
                        break;
                    case 2:
                        R = pv;
                        G = V;
                        B = tv;
                        break;

                    // Blue is the dominant color

                    case 3:
                        R = pv;
                        G = qv;
                        B = V;
                        break;
                    case 4:
                        R = tv;
                        G = pv;
                        B = V;
                        break;

                    // Red is the dominant color

                    case 5:
                        R = V;
                        G = pv;
                        B = qv;
                        break;

                    // Just in case we overshoot on our math by a little, we put these here. Since its a switch it won't slow us down at all to put these here.

                    case 6:
                        R = V;
                        G = tv;
                        B = pv;
                        break;
                    case -1:
                        R = V;
                        G = pv;
                        B = qv;
                        break;

                    // The color is not defined, we should throw an error.

                    default:
                        //LFATAL("i Value error in Pixel conversion, Value is %d", i);
                        R = G = B = V; // Just pretend its black/white
                        break;
                }
            }
            r = Clamp((int)(R * 255.0));
            g = Clamp((int)(G * 255.0));
            b = Clamp((int)(B * 255.0));

            return Color.FromArgb(255, (byte)r, (byte)g, (byte)b);
        }

        /// <summary>
        /// Clamp a value to 0-255
        /// </summary>
        static int Clamp(int i)
        {
            if (i < 0) return 0;
            if (i > 255) return 255;
            return i;
        }
    }
}