﻿using System;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace PresentChat.View.Behaviors
{
    public class ScrollLock : Behavior<ScrollViewer>
    {
        public static readonly DependencyProperty ScrollLockEnabledProperty = DependencyProperty.Register("ScrollLockEnabled", typeof(bool), typeof(ScrollLock), new PropertyMetadata(true));

        public bool ScrollLockEnabled
        {
            get { return (bool)GetValue(ScrollLockEnabledProperty); }
            set { SetValue(ScrollLockEnabledProperty, value); }
        }

        protected override void OnAttached()
        {
            AssociatedObject.Loaded += OnLoaded;
            AssociatedObject.Unloaded += OnUnLoaded;
        }


        protected override void OnDetaching()
        {
            AssociatedObject.Loaded -= OnLoaded;
            AssociatedObject.Unloaded -= OnUnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            AssociatedObject.ScrollChanged += ScrollOnChange;
            ScrollToEnd();
        }

        private void OnUnLoaded(object sender, RoutedEventArgs e)
        {
            AssociatedObject.ScrollChanged -= ScrollOnChange;
        }

        private void ScrollOnChange(object sender, ScrollChangedEventArgs e)
        {
            if (e.VerticalChange > 0) ScrollToEnd();
        }

        private void ScrollToEnd()
        {
            if (AssociatedObject == null || !ScrollLockEnabled) return;

            AssociatedObject.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.ApplicationIdle, new Action(() =>
            {
                AssociatedObject.UpdateLayout();
                AssociatedObject.ScrollToBottom();
            }));
        }
    }
}