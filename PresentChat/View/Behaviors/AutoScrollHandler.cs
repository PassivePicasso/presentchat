﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;
using System.Windows.Threading;

namespace PresentChat.View.Behaviors
{
    /// <summary>
    /// Handle auto scroll functionality
    /// </summary>
    public class AutoScrollHandler : Behavior<ScrollViewer>
    {
        private const DispatcherPriority AppIdle = DispatcherPriority.ApplicationIdle;

        public static readonly DependencyProperty AutoScrollProperty/*   */ = DependencyProperty.Register("AutoScroll", /*   */typeof(bool), /*         */typeof(AutoScrollHandler), new PropertyMetadata(false));


        private static readonly FrameworkPropertyMetadata ItemSourcePropertyMetaData = new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None, ItemsSourcePropertyChanged);
        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(IEnumerable), /*  */typeof(AutoScrollHandler), ItemSourcePropertyMetaData);

        private static void ItemsSourcePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var ash = o as AutoScrollHandler;
            ash?.ItemsSourceChanged((IEnumerable)e.OldValue, (IEnumerable)e.NewValue);

            ash?.AssociatedObject?.Dispatcher.BeginInvoke(AppIdle, new Action(() => ash.AssociatedObject.UpdateLayout()));
            ash?.AssociatedObject?.Dispatcher.BeginInvoke(AppIdle, new Action(() => ash.AssociatedObject.ScrollToBottom()));
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            var binding = new Binding("ItemsSource") { RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(ItemsControl), 1)};
            BindingOperations.SetBinding(this, ItemsSourceProperty, binding);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            BindingOperations.ClearBinding(this, ItemsSourceProperty);
        }


        public IEnumerable ItemsSource { get { return (IEnumerable)GetValue(ItemsSourceProperty); } set { SetValue(ItemsSourceProperty, value); } }
        public bool AutoScroll { get { return (bool)GetValue(AutoScrollProperty); } set { SetValue(AutoScrollProperty, value); } }

        private void ItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            var collection = oldValue as INotifyCollectionChanged;
            if (collection != null)
                collection.CollectionChanged -= CollectionChangedEventHandler;

            collection = newValue as INotifyCollectionChanged;
            if (collection != null)
                collection.CollectionChanged += CollectionChangedEventHandler;
        }

        private void CollectionChangedEventHandler(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Add || e.NewItems == null || e.NewItems.Count < 1)
                return;

            AssociatedObject?.Dispatcher.BeginInvoke(AppIdle, new Action(() => AssociatedObject.UpdateLayout()));
            AssociatedObject?.Dispatcher.BeginInvoke(AppIdle, new Action(() => AssociatedObject.ScrollToBottom()));
        }
    }
}