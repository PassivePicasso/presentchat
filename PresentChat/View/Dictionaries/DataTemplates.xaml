﻿<ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
                    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
                    xmlns:userControls="clr-namespace:PresentChat.View.UserControls"
                    xmlns:viewModel="clr-namespace:PresentChat.ViewModel"
                    xmlns:model="clr-namespace:PresentChat.Model"
                    xmlns:customControls="clr-namespace:PresentChat.View.CustomControls"
                    xmlns:contentSections="clr-namespace:PresentChat.View.UserControls.ContentSections"
                    mc:Ignorable="d">

    <ResourceDictionary.MergedDictionaries>
        <ResourceDictionary Source="Styles.xaml"/>
    </ResourceDictionary.MergedDictionaries>

    <DataTemplate x:Key="ServerTabTemplate" DataType="{x:Type model:IServer}">
        <StackPanel Orientation="Horizontal" Margin="4">
            <!-- ReSharper disable once Xaml.BindingWithContextNotResolved -->
            <Button Content="X" FontSize="10" FontWeight="Light" Foreground="Red" Background="Transparent" BorderBrush="Black" HorizontalAlignment="Right" VerticalAlignment="Top"
                            Command="{Binding RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type Window}}, Path=DataContext.RemoveServerCommand}" 
                            CommandParameter="{Binding}"/>

            <TextBlock Text="{Binding Path=ServerAddress, StringFormat={}{0}:}" Foreground="{Binding Source={x:Static viewModel:MainViewModel.LogicalRoot},Path=UserProfile.MainTheme.UiTextBrush}" IsHitTestVisible="False" Margin="2,0,0,0"/>
            <TextBlock Text="{Binding Path=ServerPort}" Foreground="{Binding Source={x:Static viewModel:MainViewModel.LogicalRoot},Path=UserProfile.MainTheme.UiTextBrush}" IsHitTestVisible="False"/>
        </StackPanel>
    </DataTemplate>

    <DataTemplate DataType="{x:Type viewModel:ChannelViewModel}">
        <Grid>
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="*"/>
                <ColumnDefinition Width="Auto"/>
            </Grid.ColumnDefinitions>
            <userControls:Channel Messages="{Binding Path=Messages, Mode=OneWay}" Topic="{Binding Path=Topic, Mode=OneWay}"/>
            <userControls:UserList Grid.Column="1" Users="{Binding Path=Users, Mode=OneWay}" ShowUsers="{Binding Path=ShowUsers, Mode=OneWay}"/>
        </Grid>
    </DataTemplate>

    <DataTemplate x:Key="ChannelTabTemplate" DataType="{x:Type model:IChannel}">
        <StackPanel Orientation="Horizontal" Background="Transparent" Name="Panel">
            <CheckBox IsChecked="{Binding Path=AutoJoin}" Foreground="Black" Margin="2" 
                                              IsEnabled="{Binding Path=CanPart}"
                                              Visibility="{Binding Path=CanAutoJoin, Converter={StaticResource FalseCollapsesConverter}}"
                                              Command="{Binding Path=ParentServer.ToggleAutoJoinCommand}"
                                              CommandParameter="{Binding}"/>

            <TextBlock x:Name="ChannelNameDisplay"
                Text="{Binding Path=ChannelName}" Foreground="{Binding Source={x:Static viewModel:MainViewModel.LogicalRoot},Path=UserProfile.MainTheme.UiTextBrush}" Margin="2"/>

            <Button Content="Ó" FontFamily="Wingdings 2" FontSize="10" Margin="2" FontWeight="Light" 
                                            Foreground="Red" Background="Transparent" BorderBrush="Black" 
                                            HorizontalAlignment="Right" VerticalAlignment="Top"
                                            Visibility="{Binding Path=CanPart, Converter={StaticResource FalseCollapsesConverter}}" 
                                            IsEnabled="{Binding Path=CanPart}"
                                            Command="{Binding Path=ParentServer.PartCommand}" 
                                            CommandParameter="{Binding Path=ChannelName}"/>
        </StackPanel>
        <DataTemplate.Triggers>
            <MultiDataTrigger>
                <MultiDataTrigger.Conditions>
                    <Condition Binding="{Binding Path=UnseenMessages}" Value="True"/>
                    <Condition Binding="{Binding Path=IsSelected, RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type ListBoxItem}}}" Value="False"/>
                </MultiDataTrigger.Conditions>
                <Setter TargetName="ChannelNameDisplay" Property="Foreground" Value="{Binding Source={x:Static viewModel:MainViewModel.LogicalRoot},Path=UserProfile.MainTheme.PendingUiTextBrush}"/>
            </MultiDataTrigger>
            <DataTrigger Binding="{Binding Path=IsSelected, RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type ListBoxItem}}}" Value="True">
                <Setter TargetName="Panel" Property="Background" Value="{Binding Source={x:Static viewModel:MainViewModel.LogicalRoot},Path=UserProfile.MainTheme.ActivatedBrush}"/>
            </DataTrigger>
        </DataTemplate.Triggers>
    </DataTemplate>


    <DataTemplate x:Key="ServerTemplate" DataType="{x:Type viewModel:ServerViewModel}">
        <Border BorderBrush="{Binding Source={x:Static viewModel:MainViewModel.LogicalRoot}, Path=UserProfile.MainTheme.BorderBrush}" BorderThickness="1" HorizontalAlignment="Stretch" Margin="2">
            <Grid Name="HeaderPanel" Margin="2">
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="Auto" />
                </Grid.RowDefinitions>
                <Grid Grid.Row="0" >
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="Auto" SharedSizeGroup="Title"     />
                        <ColumnDefinition Width="Auto" SharedSizeGroup="Nickname"  />
                        <ColumnDefinition Width="Auto" SharedSizeGroup="Address"   />
                        <ColumnDefinition Width="Auto" SharedSizeGroup="SASL"      />
                        <ColumnDefinition Width="Auto" SharedSizeGroup="Channels"  />
                        <ColumnDefinition Width="*"    SharedSizeGroup="Connection"/>
                    </Grid.ColumnDefinitions>
                    <!--Main ContentArea-->
                    <Border BorderBrush="{Binding Source={x:Static viewModel:MainViewModel.LogicalRoot}, Path=UserProfile.MainTheme.BorderBrush}" VerticalAlignment="Center"
                    Background ="{Binding Source={x:Static viewModel:MainViewModel.LogicalRoot}, Path=UserProfile.MainTheme.TextFieldBackgroundBrush}">
                        <TextBlock Text="{Binding Path=ServerAddress}" Foreground="{Binding Source={x:Static viewModel:MainViewModel.LogicalRoot},Path=UserProfile.MainTheme.UiTextBrush}"
                           Margin="2" VerticalAlignment="Center" HorizontalAlignment="Center"/>
                    </Border>

                    <userControls:LabelledTextBox Label="Nickname" Text="{Binding Path=NickName, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}" Width="100" VerticalAlignment="Bottom"
                                           Grid.Column="1"/>

                    <StackPanel Orientation="Horizontal"  Grid.Column="2">
                        <userControls:LabelledTextBox Label="Address" Text="{Binding Path=ServerAddress}" Width="100" VerticalAlignment="Bottom"/>
                        <userControls:LabelledTextBox Label="Port" Text="{Binding Path=ServerPort}" Width="50" VerticalAlignment="Bottom"/>
                    </StackPanel>

                    <StackPanel Orientation="Horizontal" Grid.Column="3">
                        <CheckBox IsChecked="{Binding Path=KeepChannelsOpen}" Content="Keep channels open" FontWeight="Bold" FontSize="10" VerticalAlignment="Bottom" Margin="2"/>
                        <CheckBox IsChecked="{Binding Path=UseSSL}" Content="Use SSL" FontWeight="Bold" FontSize="10" VerticalAlignment="Bottom" Margin="2"/>
                        <CheckBox IsChecked="{Binding Path=UseSASL}" Content="Use SASL" FontWeight="Bold" FontSize="10" VerticalAlignment="Bottom" Margin="2"/>
                        <userControls:LabelledTextBox Label="SASL Username" Text="{Binding Path=SASLAccount}" Width="100" VerticalAlignment="Bottom" Visibility="{Binding Path=UseSASL, Converter={StaticResource FalseCollapsesConverter}}" />
                        <userControls:LabelledTextBox Label="SASL Password" Text="{Binding Path=SASLPassword}" Width="100" VerticalAlignment="Bottom" Visibility="{Binding Path=UseSASL, Converter={StaticResource FalseCollapsesConverter}}" />
                    </StackPanel>
                    <StackPanel Orientation="Horizontal"  Grid.Column="4" HorizontalAlignment="Right" VerticalAlignment="Top">

                        <userControls:LabelledTextBox Label="Channel" x:Name="ChannelInputBox" MinWidth="100" VerticalAlignment="Bottom"/>
                        <!-- ReSharper disable once Xaml.BindingWithContextNotResolved -->
                        <Button Content="Join" Command="{Binding Path=JoinCommand}" CommandParameter="{Binding ElementName=ChannelInputBox, Path=Text}" VerticalAlignment="Bottom"/>
                    </StackPanel>
                    <!-- ReSharper disable Xaml.BindingWithContextNotResolved -->
                    <Grid Grid.Column="5" HorizontalAlignment="Right">
                        <Button Content="Connect"    Command="{Binding Path=ConnectCommand}"    Width="150" VerticalAlignment="Bottom" Visibility="{Binding Path=IsConnected, Converter={StaticResource TrueCollapsesConverter}}"  />
                        <Button Content="Disconnect" Command="{Binding Path=DisconnectCommand}" Width="150"  VerticalAlignment="Bottom" Visibility="{Binding Path=IsConnected, Converter={StaticResource FalseCollapsesConverter}}" />
                    </Grid>
                    <!-- ReSharper restore Xaml.BindingWithContextNotResolved -->
                </Grid>

                <contentSections:ChannelsContent x:Name="ChannelControl" Grid.Row="1" ChannelSource="{Binding Path=ServerChannels}" Height="500" HorizontalAlignment="Stretch"
                                                 ShowUsers="False"
                                  Visibility="{Binding RelativeSource={RelativeSource AncestorType={x:Type ListBoxItem}}, Path=IsSelected, Converter={StaticResource FalseCollapsesConverter}}">
                </contentSections:ChannelsContent>
                <customControls:ResizeControl Target="{Binding ElementName=ChannelControl}" Grid.Row="1" Style="{StaticResource VerticalResizer}" VerticalAlignment="Bottom"/>
            </Grid>
        </Border>
    </DataTemplate>
</ResourceDictionary>