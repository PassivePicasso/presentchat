﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Commander;

namespace PresentChat.View.UICommands
{
    public static class SelectorCommands
    {
        public class SelectNextItemCommand : ICommand
        {
            public event EventHandler CanExecuteChanged;

            public bool CanExecute(object parameter)
            {
                return parameter is Selector;
            }

            public void Execute(object parameter)
            {
                var selector = parameter as Selector;
                if (selector == null) return;
                if (selector.SelectedIndex < selector.Items.Count)
                {
                    selector.SelectedIndex++;
                }
            }
        }
        public class SelectPreviousItemCommand : ICommand
        {
            public event EventHandler CanExecuteChanged;

            public bool CanExecute(object parameter)
            {
                return parameter is Selector;
            }

            public void Execute(object parameter)
            {
                var selector = parameter as Selector;
                if (selector == null) return;
                if (selector.SelectedIndex > 0)
                {
                    selector.SelectedIndex--;
                }
            }
        }

        public static SelectNextItemCommand SelectNextItem { get; private set; }
        public static SelectPreviousItemCommand SelectPreviousItem { get; private set; }

        static SelectorCommands()
        {
            SelectNextItem = new SelectNextItemCommand();
            SelectPreviousItem = new SelectPreviousItemCommand();
        }
    }
}
