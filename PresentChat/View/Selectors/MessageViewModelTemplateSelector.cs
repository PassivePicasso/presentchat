﻿using System;
using System.Windows;
using System.Windows.Controls;
using PresentChat.ViewModel;

namespace PresentChat.View.Selectors
{
    public class MessageViewModelTemplateSelector : DataTemplateSelector
    {
        public string TemplateDictionaryKey { get; set; }

        public string DefaultTemplateKey { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var obj = App.Current.Resources[TemplateDictionaryKey];
            var dictionary = obj as ResourceDictionary;
            if (dictionary == null) throw new ArgumentException("TemplateDictionaryKey");

            var defaultTemplate = dictionary[DefaultTemplateKey] as DataTemplate;
            if (defaultTemplate == null) throw new ArgumentException("DefaultTemplateKey");

            var data = item as MessageViewModel;
            if (data == null) return defaultTemplate;

            var key = string.Format("{0}Template", data.MessageType);

            if (!dictionary.Contains(key)) return defaultTemplate;

            var template = dictionary[key] as DataTemplate;
            return template ?? defaultTemplate;
        }
    }
}
