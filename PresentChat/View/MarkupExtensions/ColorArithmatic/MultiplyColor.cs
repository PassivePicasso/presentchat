﻿using System;
using System.Windows.Markup;
using System.Windows.Media;

namespace PresentChat.View.MarkupExtensions.ColorArithmatic
{
    [MarkupExtensionReturnType(typeof(Color))]
    public class MultiplyColor : MarkupExtension
    {
        public Color A { get; set; }
        public float Coefficient { get; set; }

        public MultiplyColor(Color a, float coefficient)
        {
            A = a;
            Coefficient = coefficient;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Color.Multiply(A, Coefficient);
        }
    }
}
