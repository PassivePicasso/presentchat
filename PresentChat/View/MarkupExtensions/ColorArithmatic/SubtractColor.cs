﻿using System;
using System.Windows.Markup;
using System.Windows.Media;
using PropertyChanged;

namespace PresentChat.View.MarkupExtensions.ColorArithmatic
{
    [ImplementPropertyChanged]
    public class SubtractColor : MarkupExtension
    {
        public Color A { get; set; }
        public Color B { get; set; }

        public SubtractColor(Color A, Color B)
        {
            this.A = A;
            this.B = B;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Color.Subtract(A, B);
        }
    }
}
