﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;

namespace PresentChat.View.MarkupExtensions
{
    [MarkupExtensionReturnType(typeof(Brush))]
    public class SolidColorBrushHashCodeComputer : MarkupExtension, IValueConverter
    {
        private static Dictionary<object, SolidColorBrush> brushes = new Dictionary<object, SolidColorBrush>();
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (brushes.ContainsKey(value)) return brushes[value];

            var hash = BitConverter.GetBytes(Math.Abs(value.GetHashCode()));
            var color = Color.FromArgb(255, hash[0], hash[1], hash[2]);
            brushes[value] = new SolidColorBrush(Color.Add(color, System.Windows.Media.Colors.Gray));
            return brushes[value];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
