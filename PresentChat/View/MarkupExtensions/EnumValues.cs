﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace PresentChat.View.MarkupExtensions
{
    [MarkupExtensionReturnType(typeof(object[]))]
    public class EnumSource : MarkupExtension
    {
        public EnumSource() {
        }

        public EnumSource(Type enumType)
        {
            EnumType = enumType;
        }
 
        [ConstructorArgument("enumType")]
        public Type EnumType {
            get;
            set;
        }
 
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (EnumType == null) {
                throw new System.ArgumentException("The enumeration's type is not set.");
            }
 
            return Enum.GetValues(EnumType);
        }
    }
}
