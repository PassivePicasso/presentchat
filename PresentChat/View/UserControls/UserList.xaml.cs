﻿using System.Collections.Generic;
using System.Windows;
using PresentChat.ViewModel;

namespace PresentChat.View.UserControls
{
    /// <summary>
    /// Interaction logic for UserList.xaml
    /// </summary>
    public partial class UserList
    {
        public static readonly DependencyProperty UsersProperty = DependencyProperty.Register("Users", typeof(IList<UserViewModel>), typeof(UserList), new FrameworkPropertyMetadata(default(IList<UserViewModel>), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static readonly DependencyProperty ShowUsersProperty = DependencyProperty.Register("ShowUsers", typeof(bool), typeof(UserList), new FrameworkPropertyMetadata(default(bool), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public IList<UserViewModel> Users
        {
            get { return (IList<UserViewModel>)GetValue(UsersProperty); }
            set { SetValue(UsersProperty, value); }
        }

        public bool ShowUsers
        {
            get { return (bool)GetValue(ShowUsersProperty); }
            set { SetValue(ShowUsersProperty, value); }
        }

        public UserList()
        {
            InitializeComponent();
        }
    }
}
