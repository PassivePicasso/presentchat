﻿using System.Windows;
using System.Windows.Media;

namespace PresentChat.View.UserControls
{
    /// <summary>
    /// Interaction logic for TabSystem.xaml
    /// </summary>
    public partial class TabSystem
    {
        public static readonly DependencyProperty TabTemplateProperty = DependencyProperty.Register("TabTemplate", typeof(DataTemplate), typeof(TabSystem));
        public static readonly DependencyProperty SelectedBackgroundProperty = DependencyProperty.Register("SelectedBackground", typeof(Brush), typeof(TabSystem));

        public DataTemplate TabTemplate
        {
            get { return (DataTemplate)GetValue(TabTemplateProperty); }
            set { SetValue(TabTemplateProperty, value); }
        }

        public Brush SelectedBackground
        {
            get { return (Brush)GetValue(SelectedBackgroundProperty); }
            set { SetValue(SelectedBackgroundProperty, value); }
        }

        public TabSystem()
        {
            InitializeComponent();
        }
    }
}
