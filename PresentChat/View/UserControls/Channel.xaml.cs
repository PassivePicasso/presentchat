﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Data;
using PresentChat.ViewModel;
using Xceed.Wpf.AvalonDock.Controls;

namespace PresentChat.View.UserControls
{
    /// <summary>
    /// Interaction logic for Channel.xaml
    /// </summary>
    public partial class Channel
    {
        #region DependencyProperty registrations
        public static readonly DependencyProperty UnseenMessagesProperty = DependencyProperty.Register("UnseenMessages", typeof(bool), typeof(Channel),
                               new FrameworkPropertyMetadata(default(bool), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static readonly DependencyProperty MessagesProperty = DependencyProperty.Register("Messages", typeof(ObservableCollection<MessageViewModel>), typeof(Channel),
                               new FrameworkPropertyMetadata(default(ObservableCollection<MessageViewModel>), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static readonly DependencyProperty TopicProperty = DependencyProperty.Register("Topic", typeof(string), typeof(Channel),
                               new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        #endregion

        #region Dependency Properties

        public ObservableCollection<MessageViewModel> Messages
        {
            get { return (ObservableCollection<MessageViewModel>)GetValue(MessagesProperty); }
            set { SetValue(MessagesProperty, value); }
        }
        public string Topic
        {
            get { return (string)GetValue(TopicProperty); }
            set { SetValue(TopicProperty, value); }
        }

        #endregion

        public Channel()
        {
            InitializeComponent();
            IsVisibleChanged += Channel_IsVisibleChanged;
            GotFocus += Channel_GotFocus;
        }

        private void Channel_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            SetCurrentValue(UnseenMessagesProperty, false);
        }

        private void Channel_GotFocus(object sender, RoutedEventArgs e)
        {
            SetCurrentValue(UnseenMessagesProperty, false);
        }
    }
}
