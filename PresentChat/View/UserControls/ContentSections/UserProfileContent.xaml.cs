﻿using System.Windows.Controls;

namespace PresentChat.View.UserControls.ContentSections
{
    /// <summary>
    /// Interaction logic for UserProfileContent.xaml
    /// </summary>
    public partial class UserProfileContent 
    {
        public UserProfileContent()
        {
            InitializeComponent();
        }
    }
}
