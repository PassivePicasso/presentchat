﻿using System.Collections;
using System.Windows;
using System.Windows.Markup;

namespace PresentChat.View.UserControls.ContentSections
{
    /// <summary>
    /// Interaction logic for ServersContent.xaml
    /// </summary>
    public partial class ChannelsContent 
    {
        public static readonly DependencyProperty ChannelSourceProperty = DependencyProperty.Register("ChannelSource", typeof (IList), typeof (ChannelsContent), new PropertyMetadata(default(IList)));
        public static readonly DependencyProperty ShowUsersProperty = DependencyProperty.Register("ShowUsers", typeof (bool), typeof (ChannelsContent), new PropertyMetadata(default(bool)));

        public ChannelsContent()
        {
            InitializeComponent();
        }

        public IList ChannelSource
        {
            get { return (IList) GetValue(ChannelSourceProperty); }
            set { SetValue(ChannelSourceProperty, value); }
        }

        public bool ShowUsers
        {
            get { return (bool) GetValue(ShowUsersProperty); }
            set { SetValue(ShowUsersProperty, value); }
        }
    }
}
