﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace PresentChat.View.Converters
{
    public class ARGBColorConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length < 3) return null;

            var parsed = values.Select(v =>
            {
                byte pv;
                if (byte.TryParse(v.ToString(), out pv)) return pv;
                return (byte)255;
            }).ToList();

            if (parsed.Count < 4) parsed.Insert(0, 255);

            return Color.FromArgb(parsed[0], parsed[1], parsed[2], parsed[3]);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
