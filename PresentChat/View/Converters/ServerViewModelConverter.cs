using System;
using System.Globalization;
using System.Windows.Data;
using PresentChat.ViewModel;

namespace PresentChat.View.Converters
{
    public class ServerViewModelConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length != 2) return null;
            if (!(values[0] is string)) return null;
            if (!(values[1] is string)) return null;
            
            ushort port;
            if (!ushort.TryParse(values[1].ToString(), out port)) return null;

            return new ServerViewModel
            {
                ServerAddress = values[0].ToString(),
                ServerPort = port
            };
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            if (!(value is ServerViewModel)) return new[] { "", "" };
            var svm = value as ServerViewModel;
            return new object[] { svm.ServerAddress, svm.ServerPort };
        }
    }
}