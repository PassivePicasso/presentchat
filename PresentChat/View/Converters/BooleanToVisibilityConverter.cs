﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace PresentChat.View.Converters
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public Visibility TrueValue { get; set; }
        public Visibility FalseValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var bValue = (bool)value;
                return bValue ? TrueValue : FalseValue;
            }
            catch
            {
                return TrueValue;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.Equals(TrueValue) ? TrueValue : FalseValue;
        }
    }
}
