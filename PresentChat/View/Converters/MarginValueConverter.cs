﻿using System.Windows;
using System.Windows.Data;

namespace PresentChat.View.Converters
{
    class MarginValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var left = values.Length > 0 ? System.Convert.ToDouble(values[0] ?? 0) : 0;
            var top = values.Length > 1 ? System.Convert.ToDouble(values[1] ?? 0) : 0;
            var right = values.Length > 2 ? System.Convert.ToDouble(values[2] ?? 0) : 0;
            var bottom = values.Length > 3 ? System.Convert.ToDouble(values[3] ?? 0) : 0;
            var thickness = new Thickness(left, top, right, bottom);
            return thickness;
        }

        public object[] ConvertBack(object value, System.Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
