﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows.Input;
using Commander;
using log4net.Config;
using Microsoft.Expression.Interactivity.Core;
using Newtonsoft.Json;
using PresentChat.Annotations;
using PresentChat.Model;
using PresentChat.View.CustomControls;
using PropertyChanged;

namespace PresentChat.ViewModel
{
    [DataContract]
    [ImplementPropertyChanged]
    public class UserProfile : INotifyPropertyChanged
    {
        private static readonly JsonSerializerSettings DEFAULT_SERIALIZATION = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore,
            Formatting = Formatting.Indented,
            ObjectCreationHandling = ObjectCreationHandling.Auto,
            TypeNameHandling = TypeNameHandling.Objects,
            TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
        };

        private static readonly JsonSerializerSettings DEFAULT_DESERIALIZATION = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore,
            Formatting = Formatting.Indented,
            ObjectCreationHandling = ObjectCreationHandling.Auto,
            TypeNameHandling = TypeNameHandling.Objects,
        };

        private static List<PropertyInfo> _properties;
        private static List<PropertyInfo> SerializedProperties
        {
            get
            {
                if (_properties != null) return _properties;
                var properties = typeof(UserProfile).GetProperties();

                var serializedProperties = properties.Where(pi =>
                {
                    var dataMemberAttribute = pi.GetCustomAttribute<DataMemberAttribute>();
                    return dataMemberAttribute != null;
                });

                _properties = serializedProperties.ToList();
                return _properties;
            }
        }

        public static IEnumerable<string> AvailableProfiles
        {
            get
            {
                var results = new List<string>();

                Directory.GetFiles(App.ProfileDirectory).Where(f => f.EndsWith(".profile")).ToList().ForEach(p => results.Add(Path.GetFileNameWithoutExtension(p)));

                return results;
            }
        }

        static UserProfile()
        {
            var t = SerializedProperties;
        }

        [DataMember]
        public Theme MainTheme { get; set; }

        [DataMember]
        public string ProfileName { get; set; }

        [DataMember]
        public string DefaultNickName { get; set; }

        [DataMember]
        public string RealName { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public ObservableCollection<string> AlternateNicknames { get; set; }

        [DataMember]
        public ObservableCollection<IServer> Servers { get; set; }

        public ObservableCollection<IChannel> AllChannels { get; private set; }

        public IChannel FocusedChannel { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public UserProfile()
        {
            XmlConfigurator.Configure();
            Servers = new ObservableCollection<IServer>();
            AllChannels = new ObservableCollection<IChannel>();

            MainTheme = new Theme();
            PropertyChanged += FocusedChannelChanged;
        }

        private void FocusedChannelChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!e.PropertyName.Equals("FocusedChannel")) return;
            if (FocusedChannel == null) return;

            FocusedChannel.UnseenMessages = false;
        }

        [OnCommand("SaveCommand")]
        public void Save(UserProfile profile)
        {
            var data = JsonConvert.SerializeObject(profile, DEFAULT_SERIALIZATION);

            File.WriteAllText(Path.Combine(App.ProfileDirectory, string.Format("{0}.profile", profile.ProfileName)), data);
        }

        [OnCommand("LoadCommand")]
        public void Load(UserProfile profile)
        {
            if (string.IsNullOrEmpty(profile.ProfileName)) return;

            var readPath = Path.Combine(App.ProfileDirectory, string.Format("{0}.profile", profile.ProfileName));
            if (!File.Exists(readPath)) return;

            var deserialized = JsonConvert.DeserializeObject<UserProfile>(File.ReadAllText(readPath), DEFAULT_DESERIALIZATION);
            SerializedProperties.ForEach(pi => pi.SetValue(this, pi.GetValue(deserialized)));
        }

        public ICommand AddServerCommand => new ActionCommand(o =>
        {
            var serverViewModel = o as ServerViewModel;
            if (serverViewModel == null) return;

            serverViewModel.NickName = DefaultNickName;
            serverViewModel.Username = ProfileName;
            serverViewModel.Realname = RealName;
            Servers.Add(serverViewModel);
        });

        [OnCommand("RemoveServerCommand")]
        public void RemoveServer(ServerViewModel serverViewModel)
        {
            serverViewModel.Disconnect();
            Servers.Remove(serverViewModel);
        }


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
