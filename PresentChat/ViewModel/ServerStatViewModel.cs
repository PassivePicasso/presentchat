using System.Collections.Generic;
using System.Runtime.Serialization;
using PropertyChanged;

namespace PresentChat.ViewModel
{
    [DataContract]
    [ImplementPropertyChanged]
    public class ServerStatViewModel
    {
        public IList<string> Parameters { get; set; }
        public int StatType { get; set; }
    }
}