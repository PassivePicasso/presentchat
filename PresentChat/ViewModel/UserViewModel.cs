﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows.Documents;
using System.Windows.Media;
using PresentChat.View;
using PropertyChanged;
using DColor = System.Drawing.Color;

namespace PresentChat.ViewModel
{
    [DebuggerDisplay("{NickName} ({Ident}@{Host})")]
    [DataContract, Equals, ImplementPropertyChanged]
    public class UserViewModel
    {
        private SolidColorBrush _solidColorBrush;
        public string Mode { get; set; }

        [AlsoNotifyFor("NickColor")]
        [DataMember]
        public string NickName { get; set; }

        public string Host { get; set; }
        public string Ident { get; set; }

        private static IEnumerable<Color> colors = null;
        [IgnoreDuringEquals]
        private static IEnumerable<Color> Colors
        {
            get
            {
                if (colors != null) return colors;

                var userProfile = MainViewModel.LogicalRoot.UserProfile;
                if (userProfile == null) return new List<Color>();

                var theme = userProfile.MainTheme;
                if (theme == null) return new List<Color>();
                {
                    theme.PropertyChanged += delegate(object sender, PropertyChangedEventArgs args)
                    {
                        if (!args.PropertyName.Equals("NicknameBrush", StringComparison.CurrentCultureIgnoreCase))
                            return;
                        var sb = theme.NicknameBrush;

                        var c = DColor.FromArgb(sb.Color.A, sb.Color.R, sb.Color.G, sb.Color.B);
                        var b = c.GetBrightness();
                        var h = c.GetHue();
                        var s = c.GetSaturation();
                        colors = ColorGenerator.Generate(1, h - 30, h + 30, s - 0.1, s + 0.1, b - 0.1, b + 0.1);
                        OnRefreshColors();
                    };
                }
                var sourceBrush = theme.NicknameBrush;

                var color = DColor.FromArgb(sourceBrush.Color.A, sourceBrush.Color.R, sourceBrush.Color.G, sourceBrush.Color.B);
                var brightness = color.GetBrightness();
                var hue = color.GetHue();
                var saturation = color.GetSaturation();

                return colors = ColorGenerator.Generate(1, hue - 30, hue + 30, saturation - 0.2, saturation + 0.2, brightness - 0.2, brightness + 0.2);
            }
        }
        private static event EventHandler RefreshColors;
        private static void OnRefreshColors()
        {
            var handler = RefreshColors;
            if (handler != null) handler(typeof(UserViewModel), new EventArgs());
        }

        [IgnoreDuringEquals]
        [DependsOn("NickName", "Colors")]
        public SolidColorBrush NickColor
        {
            get
            {
                if (_solidColorBrush != null) return _solidColorBrush;

                _solidColorBrush = new SolidColorBrush(Colors.First());
                RefreshColors += (sender, args) => _solidColorBrush.Color = colors.First();

                return _solidColorBrush;
            }
        }
    }
}
