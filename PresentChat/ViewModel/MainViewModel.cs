﻿using System.Windows;
using Commander;
using PresentChat.View.CustomControls;
using PropertyChanged;

namespace PresentChat.ViewModel
{
    [ImplementPropertyChanged]
    public class MainViewModel
    {
        public static MainViewModel LogicalRoot = new MainViewModel();


        public UserProfile UserProfile { get; set; }

        public WindowState WindowState { get; set; }

        private MainViewModel()
        {
            UserProfile = new UserProfile();
        }

        [OnCommand("CloseCommand")]
        public void Close()
        {
            foreach (var serverVm in UserProfile.Servers)
                serverVm.Disconnect();

            Application.Current.Shutdown();
        }

        [OnCommand("MinimizeCommand")]
        public void Minimize()
        {
            WindowState = WindowState == WindowState.Minimized ? WindowState.Normal : WindowState.Minimized;
        }

        [OnCommand("MaximizeCommand")]
        public void Maximize()
        {
            WindowState = WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
        }

    }
}
