﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows;
using Commander;
using Meebey.SmartIrc4net;
using PresentChat.Model;
using PropertyChanged;

namespace PresentChat.ViewModel
{
    [DataContract]
    [ImplementPropertyChanged]
    public class ChannelViewModel : IChannel
    {
        /// <summary>
        /// Ignore Case
        /// </summary>
        private StringComparison i = StringComparison.CurrentCultureIgnoreCase;

        private static void AppInvoke(Action action) { Application.Current.Dispatcher.Invoke(action); }

        [DataMember]
        public string ChannelName { get; set; }

        [DataMember]
        public bool ShowUsers { get; set; }

        [DataMember]
        public bool AutoJoin { get; set; }

        public bool CanAutoJoin { get; set; }

        public bool CanPart { get; set; }

        public ObservableCollection<MessageViewModel> Messages { get; set; }

        public bool UnseenMessages { get; set; }

        public string Topic { get; set; }
        public string InputText { get; set; }

        public void AddUser(UserViewModel user) { if (!HasUser(user) && Users != null) Users.Add(user); }

        public void RemoveUser(UserViewModel user) { if (HasUser(user)) Users.Remove(user); }

        public bool HasUser(UserViewModel user) { return Users != null && Users.Any(u => u.Equals(user)); }

        public IServer ParentServer { get; set; }

        public ObservableCollection<UserViewModel> Users { get; set; }

        public ChannelViewModel()
        {
            ChannelName = string.Empty;
            Topic = string.Empty;
            Users = new ObservableCollection<UserViewModel>();
            Messages = new ObservableCollection<MessageViewModel>();

            CanAutoJoin = true;
            CanPart = true;
        }

        [OnCommand("SendMessageCommand")]
        public void SendMessage(string message)
        {
            if (string.IsNullOrEmpty(message)) return;
            var comparisonString = message.Trim().ToLower();
            var inChannel = ChannelName.StartsWith("#");

            if (comparisonString.StartsWith("/"))
            {
                var firstSpace = comparisonString.IndexOf(' ');
                var command = comparisonString.Substring(1, firstSpace).Trim().ToLower();
                var content = comparisonString.Substring(firstSpace).Trim();
                switch (command)
                {
                    case "ignore":
                        AppInvoke(() =>
                        {
                            var target = Users.FirstOrDefault(u => (!string.IsNullOrEmpty(u.NickName) && u.NickName.Equals(content, i))
                                                                    || (!string.IsNullOrEmpty(u.Host) && u.Host.Equals(content, i))
                                                                    || (!string.IsNullOrEmpty(u.Ident) && u.Ident.Equals(content, i)));
                            if (target != null) ParentServer.IgnoredUsers.Add(target);
                        });
                        break;
                    case "unignore":
                        AppInvoke(() =>
                        {
                            var target = ParentServer.IgnoredUsers.FirstOrDefault(u => (!string.IsNullOrEmpty(u.NickName) && u.NickName.Equals(content, i))
                                                                       || (!string.IsNullOrEmpty(u.Host) && u.Host.Equals(content, i))
                                                                       || (!string.IsNullOrEmpty(u.Ident) && u.Ident.Equals(content, i)));
                            if (target != null) ParentServer.IgnoredUsers.Remove(target);
                        });
                        break;
                    case "nick":
                        AppInvoke(() => ParentServer.Nickname(content));
                        break;
                    case "me":
                        AppInvoke(() => Messages.Add(new MessageViewModel { MessageType = inChannel ? ReceiveType.ChannelAction : ReceiveType.QueryAction, User = ParentServer.LocalUser, MessageContent = content }));
                        ParentServer.SendMessage(SendType.Action, ChannelName, content);
                        break;
                    case "msg":
                        AppInvoke(() => Messages.Add(new MessageViewModel { MessageType = ReceiveType.QueryMessage, User = ParentServer.LocalUser, MessageContent = message }));

                        ParentServer.SendMessage(SendType.Message, ChannelName, message);
                        break;
                    case "query":
                        break;
                }
            }
            else
            {
                var messageType = inChannel ? ReceiveType.ChannelMessage : ReceiveType.QueryMessage;

                AppInvoke(() => Messages.Add(new MessageViewModel { MessageType = messageType, User = ParentServer.LocalUser, MessageContent = message }));

                ParentServer.SendMessage(SendType.Message, ChannelName, message);
            }

            AppInvoke(() => message = string.Empty);
            InputText = string.Empty;
        }

    }
}
