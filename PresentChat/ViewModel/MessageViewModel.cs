﻿using System;
using System.Diagnostics;
using System.Windows.Media;
using Meebey.SmartIrc4net;
using PropertyChanged;

namespace PresentChat.ViewModel
{
    [DebuggerDisplay("<{User}> {MessageContent}")]
    [ImplementPropertyChanged]
    public class MessageViewModel
    {
        public DateTime TimeCreated { get; set; }
        public ReceiveType MessageType { get; set; }
        public UserViewModel User { get; set; }
        public string MessageContent { get; set; }
        public Color BackgroundColor { get; set; }
        public Color ForegroundColor { get; set; }
        public bool Seen { get; set; }

        public MessageViewModel()
        {
            TimeCreated = DateTime.Now;
            BackgroundColor = Colors.White;
            ForegroundColor = Colors.Black;
        }
    }
}
