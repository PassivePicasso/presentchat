﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Commander;
using log4net;
using log4net.Core;
using Meebey.SmartIrc4net;
using PresentChat.Model;
using PropertyChanged;
using IChannel = PresentChat.Model.IChannel;

namespace PresentChat.ViewModel
{
    [DataContract]
    [ImplementPropertyChanged]
    public class ServerViewModel : IServer
    {
        private const string ServerChanName = "Server";
        private static ILog Logger = LogManager.GetLogger(typeof(ServerViewModel));

        private static void AppInvoke(Action action) { Application.Current.Dispatcher.Invoke(action); }

        #region Serialized Properties

        [DataMember]
        public string NickName { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Realname { get; set; }

        [DataMember]
        public string Password { get; set; }
#warning this should not be stored in plain text

        [DataMember]
        public string ServerAddress { get; set; }
        [DataMember]
        public ushort ServerPort { get; set; }
        [DataMember]
        public bool UseSSL { get; set; }

        [DataMember]
        public bool UseSASL { get; set; }

        [DataMember]
        public string SASLAccount { get; set; }
        [DataMember]
        public string SASLPassword { get; set; }

        [DataMember]
        public ObservableCollection<IChannel> FavoriteChannels { get; set; }

        [DataMember]
        public ObservableCollection<UserViewModel> IgnoredUsers { get; set; }

        [DataMember]
        public bool KeepChannelsOpen { get; set; }

        #endregion

        #region Stateful Properties


        public string ServerName { get; set; }

        public bool IsConnected { get; set; }

        public bool ConnectionErrored { get; set; }

        public ObservableCollection<IChannel> ConnectedChannels { get; set; }

        public Thread ListenerThread { get; set; }

        public UserViewModel LocalUser { get; set; }

        #endregion

        #region CommandBindings

        [OnCommand("ConnectCommand")]
        public void Connect()
        {
            if (client.IsConnected) return;
            Task.Run(() => client.Connect(ServerAddress, ServerPort));
        }

        [OnCommand("DisconnectCommand")]
        public void Disconnect() { if (client.IsConnected) client.RfcQuit("Thank you for experiencing Present Chat"); }

        public void SendMessage(SendType sendType, string channel, string content) { client.SendMessage(sendType, channel, content); }

        [OnCommand("NickCommand")]
        public void Nickname(string nickname) { client.RfcNick(nickname); }

        [OnCommand("JoinCommand")]
        public void Join(string channel) { client.RfcJoin(channel); }

        [OnCommand("PartCommand")]
        public void Part(string channel) { client.RfcPart(channel); }


        [OnCommand("ToggleAutoJoinCommand")]
        public void ToggleAutoJoin(ChannelViewModel cvm)
        {
            if (cvm.AutoJoin) FavoriteChannels.Add(cvm);
            else FavoriteChannels.Remove(cvm);
        }

        #endregion

        private IrcClient client;
        private readonly UserViewModel serverUser = new UserViewModel { NickName = "Server" };
        public ChannelViewModel ServerChannel { get; set; }
        public IEnumerable<ChannelViewModel> ServerChannels { get { return new[] { ServerChannel }; } }

        /// <summary>
        /// Ignore Case
        /// </summary>
        private StringComparison i = StringComparison.CurrentCultureIgnoreCase;


        private List<Action> DisconnectedCallbacks { get; }


        public ObservableCollection<ServerStatViewModel> ServerStatistics { get; set; }

        public ServerViewModel()
        {
            FavoriteChannels = new ObservableCollection<IChannel>();
            ConnectedChannels = new ObservableCollection<IChannel>();
            ServerStatistics = new ObservableCollection<ServerStatViewModel>();
            IgnoredUsers = new ObservableCollection<UserViewModel>();

            DisconnectedCallbacks = new List<Action>();

            ServerName = ServerAddress;

            if (ServerChannel == null)
            {
                ServerChannel = new ChannelViewModel { ChannelName = ServerChanName, ShowUsers = false, Topic = ServerName, CanAutoJoin = false, CanPart = false, ParentServer = this };
                ConnectedChannels.Insert(0, ServerChannel);
            }

            InitializeClient();
        }


        [OnDeserialized]
        public void DeserializedHandled(StreamingContext context)
        {
            ConnectedChannels.CollectionChanged += (sender, args) =>
            {
                if (args.OldItems != null)
                    foreach (var channel in args.OldItems.OfType<IChannel>())
                        MainViewModel.LogicalRoot.UserProfile.AllChannels.Remove(channel);

                if (args.NewItems == null) return;
                foreach (var channel in args.NewItems.OfType<IChannel>())
                {
                    channel.ParentServer = this;
                    MainViewModel.LogicalRoot.UserProfile.AllChannels.Add(channel);
                }
            };
        }


        #region Model to ViewModel Message Conversion

        public void LogChannelMessage(ReceiveType messageType, string message, string channelName, string nickname, string host, string ident)
        {

            var target = IgnoredUsers.FirstOrDefault(u => (!string.IsNullOrEmpty(u.NickName) && u.NickName.ToLower().Equals(nickname.ToLower()))
                                                       || (!string.IsNullOrEmpty(u.Host) && u.Host.ToLower().Equals(host.ToLower()))
                                                       || (!string.IsNullOrEmpty(u.Ident) && u.Ident.ToLower().Equals(ident.ToLower())));
            if (target != null) return;

            AppInvoke(() =>
            {
                try
                {
                    var channel = ConnectedChannels.FirstOrDefault(cc => cc.ChannelName.Equals(channelName, i));
                    if (channel == null)
                        ConnectedChannels.Add(channel = new ChannelViewModel { ChannelName = channelName ?? "Unknown Channel", });

                    var user = channel.Users.Where(cu => !string.IsNullOrEmpty(cu.NickName)).FirstOrDefault(cu => cu.NickName.Equals(nickname, i))
                            ?? new UserViewModel { NickName = nickname, Host = host, Ident = ident };

                    channel.Messages.Add(new MessageViewModel { MessageType = messageType, User = user, MessageContent = message });
                    if (MainViewModel.LogicalRoot.UserProfile.FocusedChannel != channel)
                        channel.UnseenMessages = true;
                }
                catch (Exception e)
                {
                    Console.Write("Failed to log message", e);
                }
            });
        }

        private void LogServerMessage(string message, Level level = null, Exception exception = null)
        {
            AppInvoke(() => ServerChannel.Messages.Add(new MessageViewModel { MessageContent = message, User = serverUser }));
        }

        #endregion

        private void InitializeClient()
        {
            if (client != null)
            {
                client.Disconnect();
            }
            client = new IrcClient
            {
                SendDelay = 200,
                AutoRetry = true,
                Encoding = Encoding.UTF8,
            };

            Application.Current.Exit += OnApplicationExit;

            #region Connection Events
            client.OnDisconnected += ClientOnDisconnected;
            client.OnConnected += ClientOnConnected;
            client.OnDisconnecting += ClientOnOnDisconnecting;
            client.OnConnecting += ClientOnOnConnecting;
            client.OnConnectionError += ClientOnConnectionError;
            #endregion

            client.OnQuit += ClientOnOnQuit;


            #region Server Data Events
            client.OnMotd += ClientOnMotdReceived;

            client.OnPing += ClientOnOnPing;
            #endregion

            #region User Events

            client.OnNickChange += OnNickChange;
            #endregion

            #region Channel Events
            client.OnJoin += ClientOnOnJoin;
            client.OnPart += ClientOnOnPart;

            client.OnTopic += ClientOnOnTopic;
            client.OnTopicChange += ClientOnOnTopicChange;

            client.OnChannelAction += ClientOnOnChannelAction;
            client.OnChannelMessage += ClientOnOnChannelMessage;
            #endregion

            #region Query Events
            client.OnQueryMessage += ClientOnOnQueryMessage;
            client.OnQueryAction += ClientOnOnQueryAction;
            #endregion

            #region Miscelanous IrcClient Events
            client.OnErrorMessage += ClientOnOnErrorMessage;
            client.OnRawMessage += OnMessage;
            #endregion
        }

        private void OnApplicationExit(object sender, ExitEventArgs exitEventArgs)
        {
            if (client.IsConnected)
                client.Disconnect();
        }

        #region Connection Event Handlers

        private void StartListener()
        {
            ListenerThread = new Thread(clientParam =>
            {
                var ircClient = clientParam as IrcClient;
                while (ircClient != null && IsConnected)
                {
                    ircClient.Listen();
                    Thread.Sleep(1);
                }
            });
            ListenerThread.Start(client);
        }

        private void ClientOnOnDisconnecting(object sender, EventArgs eventArgs)
        {
            LogServerMessage(string.Format("Disconnecting from {0}:{1}", ServerAddress, ServerPort));
        }

        private void ClientOnDisconnected(object sender, EventArgs eventArgs)
        {
            LogServerMessage(string.Format("Disconnected from {0}:{1}", ServerAddress, ServerPort));

            AppInvoke(() => IsConnected = false);

            DisconnectedCallbacks.ForEach(callback => callback.Invoke());

            if (!KeepChannelsOpen)
                AppInvoke(() => ConnectedChannels.Except(new[] { ServerChannel }).ToList().ForEach(cc => ConnectedChannels.Remove(cc)));
        }

        private void ClientOnOnQuit(object sender, QuitEventArgs e)
        {
            var userchannels = ConnectedChannels.Where(cc => cc.Users.Any(u => !string.IsNullOrEmpty(u.NickName) && u.NickName.ToLower().Equals(e.Who.ToLower())));
            userchannels.ToList().ForEach(chan => LogChannelMessage(ReceiveType.Quit, string.Format("{0} has quit ({1})", e.Who, e.QuitMessage), chan.ChannelName, e.Who, null, null));
        }

        private void ClientOnOnConnecting(object sender, EventArgs eventArgs)
        {
            LogServerMessage(string.Format("Connecting to {0}:{1}", ServerAddress, ServerPort));
        }

        private void ClientOnConnected(object sender, EventArgs eventArgs)
        {
            LogServerMessage(string.Format("Connected to {0}:{1}", ServerAddress, ServerPort));
            AppInvoke(() =>
            {
                try
                {
                    IsConnected = true;
                    StartListener();
                    client.Login(NickName, Realname, 4, Username);
                    client.RfcJoin(FavoriteChannels.Select(fc => fc.ChannelName).ToArray());
                }
                catch (Exception e)
                {
                    LogServerMessage("Error occurred! Message: " + e.Message, Level.Error, e);
                }
            });
        }

        private void ClientOnConnectionError(object sender, EventArgs args)
        {
            LogServerMessage(string.Format("Connection error on {0}:{1}", ServerAddress, ServerPort), Level.Error);
            AppInvoke(() =>
            {
                ConnectionErrored = true;
                IsConnected = false;
            });
        }

        #endregion

        #region Server Data Event Handlers

        private void ClientOnMotdReceived(object sender, MotdEventArgs eventArgs) { LogServerMessage(eventArgs.MotdMessage); }

        private void ClientOnOnPing(object sender, PingEventArgs pingEventArgs) { LogServerMessage(pingEventArgs.PingData); }

        #endregion

        #region User Events

        private void OnNickChange(object sender, NickChangeEventArgs args)
        {
            if (args.OldNickname.Equals(NickName, i))
            {
                NickName = args.NewNickname;
                LocalUser.NickName = NickName;
                LogServerMessage(string.Format("Nickname changed from {0} to {1}", args.OldNickname, args.NewNickname));
            }
            else
            {
                var channelUsers = ConnectedChannels.SelectMany(cc => cc.Users.Where(cu => cu.NickName.Equals(args.OldNickname, i)));
                channelUsers.ToList().ForEach(cu => cu.NickName = args.NewNickname);
            }
        }

        #endregion

        #region Channel Event Handlers

        private void ClientOnOnJoin(object sender, JoinEventArgs args)
        {
            var channel = args.Channel ?? string.Empty;
            var nickname = args.Who ?? string.Empty;
            if (nickname.Equals(NickName))
            {
                LogServerMessage(string.Format("Joined {2} on {0}:{1}", ServerAddress, ServerPort, channel));
                AppInvoke(() =>
                {
                    var chanVm = FavoriteChannels.FirstOrDefault(fc => channel.Equals(fc.ChannelName, i)) ?? new ChannelViewModel { ChannelName = channel ?? string.Empty, };
                    ConnectedChannels.Add(chanVm);
                });
            }
            else
            {
                AppInvoke(() =>
                {
                    var channelVm = ConnectedChannels.FirstOrDefault(cc => channel.Equals(cc.ChannelName, i));
                    var userViewModel = new UserViewModel { NickName = nickname, Ident = args.Data.Ident, Host = args.Data.Host };
                    if (channelVm != null && !channelVm.HasUser(userViewModel))
                        channelVm.Users.Add(userViewModel);
                });
                LogChannelMessage(args.Data.Type, string.Format("({2}) {0} has joined {1}", nickname, channel, args.Data.Host), channel, nickname, null, null);
            }
        }

        private void ClientOnOnPart(object sender, PartEventArgs args)
        {
            var channel = args.Channel;
            var nickname = args.Who;
            if (nickname.Equals(NickName))
            {
                LogServerMessage(string.Format("Parted {2} on {0}:{1}", ServerAddress, ServerPort, channel));
                AppInvoke(() => ConnectedChannels.Where(cc => channel.Equals(cc.ChannelName)).ToList().ForEach(cc => ConnectedChannels.Remove(cc)));
            }
            else
            {
                LogChannelMessage(args.Data.Type, string.Format("({2}) {0} has parted {1} - {3}", nickname, channel, args.Data.Host, args.PartMessage), channel, nickname, null, null);
                AppInvoke(() =>
                {
                    var channelVm = ConnectedChannels.FirstOrDefault(cc => channel.Equals(cc.ChannelName, i));
                    if (channelVm == null) return;
                    foreach (var user in channelVm.Users.Where(u => u.NickName.Equals(args.Data.Nick.TrimStart('@', '+'), i)).ToList())
                        channelVm.Users.Remove(user);
                });
            }
        }

        private void ClientOnOnChannelAction(object sender, ActionEventArgs actionArgs)
        {
            LogChannelMessage(actionArgs.Data.Type, actionArgs.ActionMessage, actionArgs.Data.Channel, actionArgs.Data.Nick, actionArgs.Data.Host, actionArgs.Data.Ident);
        }

        private void ClientOnOnChannelMessage(object sender, IrcEventArgs ircEventArgs)
        {
            var data = ircEventArgs.Data;
            LogChannelMessage(data.Type, data.Message, data.Channel, data.Nick, data.Host, data.Ident);
        }

        private void ClientOnOnTopicChange(object sender, TopicChangeEventArgs topicEvent)
        {
            var channelVm = ConnectedChannels.FirstOrDefault(cc => topicEvent.Channel.Equals(cc.ChannelName, i));
            if (channelVm == null) return;

            AppInvoke(() => channelVm.Topic = topicEvent.NewTopic);
        }

        private void ClientOnOnTopic(object sender, TopicEventArgs topicEvent)
        {
            var channelVm = ConnectedChannels.FirstOrDefault(cc => topicEvent.Channel.Equals(cc.ChannelName, i));
            if (channelVm == null) return;

            AppInvoke(() => channelVm.Topic = topicEvent.Topic);
        }

        #endregion

        #region Query Event Handlers

        private void ClientOnOnQueryMessage(object sender, IrcEventArgs ircEventArgs)
        {
            var data = ircEventArgs.Data;
            LogChannelMessage(data.Type, data.Message, data.Nick, data.Nick, data.Host, data.Ident);

        }

        private void ClientOnOnQueryAction(object sender, ActionEventArgs actionArgs)
        {
            LogChannelMessage(actionArgs.Data.Type, actionArgs.ActionMessage, actionArgs.Data.Nick, actionArgs.Data.Nick, actionArgs.Data.Host, actionArgs.Data.Ident);
        }


        #endregion

        #region Miscelanous IrcClient Event Handlers
        private void ClientOnOnErrorMessage(object sender, IrcEventArgs ircEventArgs)
        {
            LogServerMessage(ircEventArgs.Data.Message);
        }

        private void OnMessage(object sender, IrcEventArgs ircEventArgs)
        {
            var data = ircEventArgs.Data;
            switch (data.Type)
            {
                #region Message Handling
                case ReceiveType.Name:
                    AppInvoke(() => ConnectedChannels.ToList().ForEach(cc =>
                    {
                        if (string.IsNullOrEmpty(cc.ChannelName) || !cc.ChannelName.Equals(data.Channel, i)) return;
                        var users = data.MessageArray.Select(message => new UserViewModel { NickName = message })
                            .Where(uvm => !cc.Users.Any(u => u.NickName.Equals(uvm.NickName)))
                            .ToList();
                        foreach (var user in users)
                        {
                            if (user.NickName.ToLower().Equals(NickName.ToLower())) LocalUser = user;

                            cc.Users.Add(user);
                        }
                    }));
                    break;
                    #endregion
            }
        }
        #endregion
    }
}
