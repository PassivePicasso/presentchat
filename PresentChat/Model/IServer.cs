﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Commander;
using Meebey.SmartIrc4net;
using PresentChat.ViewModel;

namespace PresentChat.Model
{
    public interface IServer
    {
        [DataMember]
        string NickName { get; set; }

        [DataMember]
        string Username { get; set; }

        [DataMember]
        string Email { get; set; }

        [DataMember]
        string Realname { get; set; }

        [DataMember]
        string Password { get; set; }

        [DataMember]
        string ServerAddress { get; set; }

        [DataMember]
        ushort ServerPort { get; set; }

        [DataMember]
        bool UseSSL { get; set; }

        [DataMember]
        ObservableCollection<IChannel> FavoriteChannels { get; set; }

        string ServerName { get; set; }
        bool IsConnected { get; set; }
        bool ConnectionErrored { get; set; }
        ObservableCollection<IChannel> ConnectedChannels { get; set; }
        
        ChannelViewModel ServerChannel { get; set; }

        [DataMember]
        ObservableCollection<UserViewModel> IgnoredUsers { get; set; }

        UserViewModel LocalUser { get; set; }

        void Connect();

        void Disconnect();
        void SendMessage(SendType sendType, string channel, string content);
        void Nickname(string newname);

    }
}