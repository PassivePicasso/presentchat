using System;
using System.Collections.ObjectModel;
using PresentChat.Annotations;
using PresentChat.ViewModel;

namespace PresentChat.Model
{
    public interface IChannel
    {
        IServer ParentServer { get; set; }
        ObservableCollection<UserViewModel> Users { get; set; }
        ObservableCollection<MessageViewModel> Messages { get; set; }
        bool UnseenMessages { get; set; }
        bool ShowUsers { get; set; }
        bool AutoJoin { get; set; }
        bool CanAutoJoin { get; }
        bool CanPart { get; }
        string ChannelName { get; set; }
        string Topic { get; set; }
        string InputText { get; set; }

        void AddUser(UserViewModel user);
        void RemoveUser(UserViewModel user);
        bool HasUser(UserViewModel user);
        void SendMessage(string channel);
    }
}